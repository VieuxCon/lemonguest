import {StaticHelpers} from "./StaticHelpers";
import Optional from "optional-js";


export const ArrowDirection = {
    Left: 37,
    Up: 38,
    Right: 39,
    Down: 40
};

export class AppEngine {
    constructor() {
    }

    static computeHistoryPosition(history, currentPosition, direction) {

        if (direction === ArrowDirection.Up && currentPosition < history.length) {
            return currentPosition + 1;
        } else if (direction === ArrowDirection.Down && currentPosition > 0) {
            return currentPosition - 1;
        }
        return currentPosition;
    }

    static computeProposalsIndexWithArrow(currentPosition, proposals, direction, columnSize) {
        columnSize = 4;
        if (proposals.length > 0) {
            switch (direction) {
                case ArrowDirection.Left:
                    return Math.max(currentPosition - 1, 0);
                case ArrowDirection.Right:
                    return Math.min(currentPosition + 1, proposals.length - 1);
                case ArrowDirection.Up:
                    return currentPosition - columnSize >= 0 ? Math.max(currentPosition - columnSize, 0) : currentPosition;
                case ArrowDirection.Down:
                    return currentPosition + columnSize <= proposals.length - 1 ? Math.min(currentPosition + columnSize, proposals.length - 1) : currentPosition;
            }
        }
        return 0;
    }

    static computeProposalsIndexWithTab(proposalsIndex, proposalsDisplayed) {
        if (proposalsDisplayed.length === 0) {
            return Optional.empty();
        }
        return Optional.of((proposalsIndex.orElse(-1) + 1) % proposalsDisplayed.length);
    }

    static async computeProposals(toExecuteOption) {
        if (!toExecuteOption.isPresent()) {
            return [];
        } else {
            return await toExecuteOption.get().callback;
        }
    }

    static computeCurrentCommand(currentInputValue) {
        const values = currentInputValue.trim().split(" ").filter(f => f !== "");
        return values.length > 0 ? Optional.of(values[0].toLowerCase()) : Optional.empty();
    }

    static computeCurrentArguments(currentInputValue) {
        return currentInputValue.trim().split(" ").filter(f => f !== "").slice(1);
    }

    static computeProposalsDisplayed(proposals, currentInputValue) {

        const currentCommand = AppEngine.computeCurrentCommand(currentInputValue);
        const currentArguments = AppEngine.computeCurrentArguments(currentInputValue);

        return currentCommand.flatMap(command => {

            if (currentArguments.length === 1) {
                return Optional.of(proposals.filter(f => f.startsWith(currentArguments[0])));
            }

            if (currentArguments.length === 0) {
                if (currentInputValue.endsWith(" ")) {
                    return Optional.of(proposals);
                }
                return Optional.of(proposals.filter(f => f.startsWith(command)));
            }

            return Optional.of(proposals);
        }).orElse(proposals);
    }

    static transformInputBangBang(currentInputValue, optionalPreviousCommand) {
        const currentCommand = AppEngine.computeCurrentCommand(currentInputValue);
        const currentArgs = AppEngine.computeCurrentArguments(currentInputValue);

        return currentCommand.flatMap((command) => {
            if (command === "!!") {
                return Optional.of("");
            }
            if (currentArgs[0] === "!!") {
                if (optionalPreviousCommand.isPresent()) {
                    let previousCommand = optionalPreviousCommand.get();
                    if (previousCommand.args.length > 0) {
                        return Optional.of(command + " " + previousCommand.args.join(" "));
                    }
                    return Optional.of(command);
                } else {
                    return Optional.of(command);
                }
            }
            return Optional.of(command);
        }).orElse("");
    }

    static transformInputAutocomplete(currentInputValue, proposalSelected) {

        const currentCommand = AppEngine.computeCurrentCommand(currentInputValue);
        const currentArguments = AppEngine.computeCurrentArguments(currentInputValue);

        return currentCommand.flatMap(command => {
            if (proposalSelected === "") {
                return Optional.of(command);
            }


            if (currentArguments.length === 0) {
                if (currentInputValue.endsWith(" ")) {
                    return Optional.of(command + " " + proposalSelected);
                }
                return Optional.of(proposalSelected);
            } else {
                return Optional.of(command + currentArguments.slice(0, currentArguments.length - 1).join(" ") + " " + proposalSelected);
            }

        }).orElse(proposalSelected);
    }

    static findAutocompleteParameters(currentInputValue) {
        let autocompleteParameters = StaticHelpers.autocompleteParameters();
        return Optional.ofNullable(autocompleteParameters.find(f => f.entryPoint.test(currentInputValue)));
    }
}