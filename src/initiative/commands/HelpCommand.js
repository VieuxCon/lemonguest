import {StaticHelpers} from "../../initiative/StaticHelpers";
import Engine from "../Engine";

export default class HelpCommand {

    name = "help";

    async execute(args) {
        if (args.length === 0) {
            return {
                name: this.name,
                args: args,
                output: StaticHelpers.COMMANDS().map(c => c.name),
                templateName: "help"
            };
        } else {
            try {
                let markdown = await Engine.getHelp(args[0]);
                return {
                    name: this.name,
                    args: args,
                    output: markdown,
                    templateName: "markdown"
                };
            } catch (e) {
                console.error(e);
                return {
                    name: this.name,
                    args: args,
                    output: args[0] + " is not documented or does not exists.",
                    templateName: "error"
                };
            }
        }
    }

}