import Engine from "../Engine";

export default class RemoveCommand {

    name = "remove";

    async execute(args) {
        if (args.length < 1) {
            return {
                name: this.name,
                args: args,
                output: "missing parameter (e.g.: remove adrien)",
                templateName: "error"
            };
        } else {
            const name = args[0];
            return Engine.remove(name)
                .then(found => {
                    return {name: this.name, args: args, output: name + " has been removed from the encounter.", templateName: "default"};
                })
                .catch(error => {
                    let statusCode = error.status;
                    switch (statusCode) {
                        case 404 :
                            return {
                                name: this.name,
                                args: args,
                                output: name + " does not exists in the encounter.",
                                templateName: "error"
                            };
                        default:
                            return {
                                name: this.name,
                                args: args,
                                output: "Unexpected error " + statusCode,
                                templateName: "error"
                            };
                    }
                })
        }
    }
}