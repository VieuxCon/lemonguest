import Engine from "../Engine";

export default class SetInitiativeCommand {

    name = "set-init";

    async execute(args) {
        if (args.length < 2) {
            return {
                name: this.name,
                args: args,
                output: "missing parameters (e.g: set-init adrien 12)",
                templateName: "error"
            };
        } else {
            const name = args[0];
            const value = parseInt(args[1]);
            if (isNaN(value)) {
                return {
                    name: this.name,
                    args: args,
                    output: 'The request should be like "set-init adrien 12".',
                    templateName: "error"
                };
            }
            return Engine.setInitiative(name, value)
                .then(encounter => {
                    return {
                        name: this.name,
                        args: args,
                        output: name + "'s initiative has been set to " + value + ".",
                        templateName: "default"
                    };
                })
                .catch(error => {
                    let statusCode = error.status;
                    switch (statusCode) {
                        case 404 :
                            return {
                                name: this.name,
                                args: args,
                                output: name + " does not exists in the encounter.",
                                templateName: "error"
                            };
                        default:
                            return {
                                name: this.name,
                                args: args,
                                output: "Unexpected error " + statusCode,
                                templateName: "error"
                            };
                    }
                })
        }
    }
}