import Engine from "../Engine";

export default class GetMonsterCommand {

    name = "monster";

    async execute(args) {
        if (args.length < 1) {
            return {
                name: this.name,
                args: args,
                output: "missing parameter (e.g.: monster adrien)",
                templateName: "error"
            };
        } else {
            const name = args[0];
            return Engine.getMonsterByName(name)
                .then(found => {
                    return {name: this.name, args: args, output: found, templateName: "monster"};
                })
                .catch(error => {
                    let statusCode = error.status;
                    switch (statusCode) {
                        case 404 :
                            return {
                                name: this.name,
                                args: args,
                                output: name + " is not in the encounter.",
                                templateName: "error"
                            };
                        default:
                            return {
                                name: this.name,
                                args: args,
                                output: "Unexpected error " + statusCode,
                                templateName: "error"
                            };
                    }
                })
        }
    }
}