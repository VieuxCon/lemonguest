import Engine from "../Engine";

export default class HealCommand {

    name = "heal";

    async execute(args) {
        if (args.length < 2) {
            return {
                name: this.name,
                args: args,
                output: "missing parameters (e.g.: heal adrien 2)",
                templateName: "error"
            };
        } else {
            const name = args[0];
            const monsterDamage = parseInt(args[1]);
            if (isNaN(monsterDamage)) {
                return {
                    name: this.name,
                    args: args,
                    output: 'The request should be like "heal adrien 2".',
                    templateName: "error"
                };
            }
            return Engine.damage(name, monsterDamage)
                .then(found => {
                    return {name: this.name, args: args, output: found, templateName: "monster"};
                })
                .catch(error => {
                    let statusCode = error.status;
                    switch (statusCode) {
                        case 404 :
                            return {
                                name: this.name,
                                args: args,
                                output: name + " does not exists in the encounter.",
                                templateName: "error"
                            };
                        default:
                            return {
                                name: this.name,
                                args: args,
                                output: "Unexpected error " + statusCode,
                                templateName: "error"
                            };
                    }
                })
        }
    }
}