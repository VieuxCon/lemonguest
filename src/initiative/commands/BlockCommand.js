import Engine from "../Engine";

export default class BlockCommand {

    name = "block";

    async execute(args) {
        if (args.length < 1) {
            return {
                name: this.name,
                args: args,
                output: "missing parameter (e.g.: block goblin)",
                templateName: "error"
            };
        } else {
            const blockName = args[0];
            return Engine.getBlockByName(blockName)
                .then(block => {
                    return {name: this.name, args: args, output: block, templateName: "block"};
                })
                .catch(error => {
                    let statusCode = error.status;
                    switch (statusCode) {
                        case 404 :
                            return {
                                name: this.name,
                                args: args,
                                output: blockName + " is not registered.",
                                templateName: "error"
                            };
                        default:
                            return {
                                name: this.name,
                                args: args,
                                output: "Unexpected error " + statusCode,
                                templateName: "error"
                            };
                    }
                })
        }
    }
}