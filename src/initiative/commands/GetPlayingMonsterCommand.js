import Engine from "../Engine";

export default class GetPlayingMonsterCommand {

    name = "playing";

    async execute(args) {
        const blockName = args[0];
        return Engine.getPlayingMonster(blockName)
            .then(found => {
                return {name: this.name, args: args, output: found, templateName: "monster"};
            })
            .catch(error => {
                let statusCode = error.status;
                switch (statusCode) {
                    case 404 :
                        return {
                            name: this.name,
                            args: args,
                            output: "No one rolled initiative.",
                            templateName: "default"
                        };
                    default:
                        return {
                            name: this.name,
                            args: args,
                            output: "Unexpected error " + statusCode,
                            templateName: "error"
                        };
                }
            })
    }
}