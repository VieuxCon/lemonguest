import Engine from "../Engine";

export default class EditionCommand {

    name = "edition";

    async execute(args) {
        let output = await Engine.getBlockByName("adult_silver_dragon");
        console.log(output);
        return {
            name: this.name,
            args: args,
            output: output,
            templateName: "editable-block"
        };
    }

}
