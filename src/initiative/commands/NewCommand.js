import Engine from "../Engine";

export default class NewCommand {

    name = "new";

    async execute(args) {
        if (args.length < 2) {
            return {
                name: this.name,
                args: args,
                output: "missing parameters (e.g.: new goblin adrien)",
                templateName: "error"
            };
        } else {
            const monsterType = args[0];
            const monsterName = args[1];
            return await Engine.newMonster(monsterName, monsterType).then(encounter => {
                return {
                    name: this.name,
                    args: args,
                    output: monsterName + " has been added to the encounter.",
                    templateName: "default"
                };
            }).catch(error => {
                let statusCode = error.status;
                switch (statusCode) {
                    case 404 :
                        return {
                            name: this.name,
                            args: args,
                            output: monsterType + " does not exists.",
                            templateName: "error"
                        };
                    case 400 :
                        return {
                            name: this.name,
                            args: args,
                            output: monsterName + " already exists in the encounter.",
                            templateName: "error"
                        };
                    default:
                        return {
                            name: this.name,
                            args: args,
                            output: "Unexpected Error " + statusCode,
                            templateName: "error"
                        };
                }
            });
        }
    }
}