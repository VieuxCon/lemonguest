import Engine from "../Engine";

export default class RollInitiativeCommand {

    name = "initiative";

    async execute(args) {
        return Engine.rollInitiative()
            .then(encounter => {
                return {name: this.name, args: args, output: "initiative rolled.", templateName: "default"};
            })
            .catch(error => {
                let statusCode = error.status;
                switch (statusCode) {
                    case 404 :
                        return {
                            name: this.name,
                            args: args,
                            output: "No one can roll initiative in the encounter.",
                            templateName: "default"
                        };
                    default:
                        return {
                            name: this.name,
                            args: args,
                            output: "Unexpected error " + statusCode,
                            templateName: "error"
                        };
                }
            })
    }
}