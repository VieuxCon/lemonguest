import {StaticHelpers} from "../StaticHelpers";

export default class ClearCommand {

    name = "clear";

    async execute(args) {
        StaticHelpers.application().commands = [];
        return {
            name: this.name,
            args: args,
            output: "all is cleared",
            templateName: "default"
        };
    }

}