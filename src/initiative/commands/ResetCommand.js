import Engine from "../Engine";

export default class ResetCommand {

    name = "reset";

    async execute(args) {
        Engine.reset();
        return {
            name: this.name,
            args: args,
            output: "The encounter is empty.",
            templateName: "default"
        };
    }

}