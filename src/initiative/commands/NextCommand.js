import Engine from "../Engine";
import {StaticHelpers} from "../StaticHelpers";

export default class NextCommand {

    name = "next";

    async execute(args) {
        await Engine.nextTurn();
        let playingCommand = StaticHelpers.COMMANDS().find(f => f.name === "playing");
        return playingCommand.execute(args)
    }
}