import $ from 'jquery'

export default class Engine {

    static server = process.env.NODE_ENV === "production" ?
        `https://storm.florentpastor.com/` : `http://localhost:9000/`;

    static async callServer(method, route, body) {
        return await $.ajax({
            contentType: "application/json",
            method: method,
            url: Engine.server + route,
            crossDomain: true,
            data: JSON.stringify(body)
        });
    }

    static async newMonster(name, blockName) {
        return await this.callServer("POST", "create", {"name": name, "blockName": blockName})
    }

    static async getBlocks() {
        return await this.callServer("GET", "blocks", {})
    }

    static async getEncounterData() {
        return await this.callServer("GET", "", {})
    }

    static async getHelp(name) {
        return await this.callServer("POST", "help", {"name": name});
    }

    static async getMonsterByName(name) {
        return await this.callServer("POST", "getByName", {"name": name});
    }

    static async getBlockByName(name) {
        return await this.callServer("POST", "block", {"name": name})
    }

    static async reset() {
        return await this.callServer("POST", "reset", {});
    }

    static async getPlayingMonster() {
        return await this.callServer("GET", "playing", {});
    }

    static async getPlayingMonsterName() {
        return await this.callServer("GET", "playingName", {});
    }

    static async rollInitiative() {
        return await this.callServer("POST", "initiative", {});
    }

    static async nextTurn() {
        await this.callServer("POST", "next", {});
        return await this.getPlayingMonster()
    }

    static async damage(name, damage) {
        await this.callServer("POST", "damage", {"name": name, "damage": damage});
        return await this.getMonsterByName(name)
    }

    static async getTurn() {
        return await this.callServer("GET", "turn", {});
    }

    static async remove(name) {
        return await this.callServer("POST", "remove", {"name": name});
    }

    static async setInitiative(name, value) {
        return await this.callServer("POST", "setInitiative", {"name": name, "value": value});
    }

    static async updateMonster(monster) { //todo
        /*if (monster == null) {
            throw new Error("Missing monster.")
        }
        this.engine.updateMonster(JSON.stringify(monster));*/
    }

}
