import Optional from "optional-js";
import Engine from "./Engine";
import BlockCommand from "./commands/BlockCommand";
import HelpCommand from "./commands/HelpCommand";
import ClearCommand from "./commands/ClearCommand";
import NewCommand from "./commands/NewCommand";
import ResetCommand from "./commands/ResetCommand";
import RollInitiativeCommand from "./commands/RollInitiativeCommand";
import GetPlayingMonsterCommand from "./commands/GetPlayingMonsterCommand";
import NextCommand from "./commands/NextCommand";
import GetMonsterCommand from "./commands/GetMonsterCommand";
import DamageCommand from "./commands/DamageCommand";
import HealCommand from "./commands/HealCommand";
import RemoveCommand from "./commands/RemoveCommand";
import SetInitiativeCommand from "./commands/SetInitiativeCommand";
import EditionCommand from "./commands/EditionCommand";

export class StaticHelpers {

    static scrollWindow() {
        let container = document.getElementById('inputLine');
        if (container != null) {
            container.scrollIntoView();
        }
    }

    static getCommands() {
        return StaticHelpers.COMMANDS().map(c => c.name).sort();
    }

    static async getBlocks() {
        return await Engine.getBlocks()
    }

    static async getMonsters() {
        return Engine.getEncounterData().then(d => d.monsters.map(m => m.name));
    }

    static async getReleases() {
        return ["linux", "windows", "macOS"]
    }

    static application() {
        return window.app;
    }

    static COMMANDS() {
        return [
            new ClearCommand,
            new HelpCommand,
            new BlockCommand,
            new NewCommand,
            new GetMonsterCommand,
            // new GetEncounterDataCommand,
            new GetPlayingMonsterCommand,
            new RollInitiativeCommand,
            new DamageCommand,
            new HealCommand,
            new NextCommand,
            new ResetCommand,
            // new GetTurnCommand,
            new RemoveCommand,
            new SetInitiativeCommand,
            new EditionCommand,
            // new ExportEncounterCommand,
            // new LoadEncounterCommand,
            // new ExportBlocksCommand,
            // new LoadBlocksCommand,
            // new IdeCommand,
            // new DeleteBlockCommand,
            // new ElectronCommand
        ];
    }

    static async evaluate(command, additionalArgs) {
        let commandFound = Optional.ofNullable(StaticHelpers.COMMANDS().find(f => f.name === command));
        if (!commandFound.isPresent()) {
            return {
                name: command,
                args: additionalArgs,
                output: "Command does not exists.",
                templateName: "error"
            };
        } else {
            return await commandFound.get().execute(additionalArgs);
        }
    }

    static autocompleteParameters() {
        return [
            {entryPoint: new RegExp("^[a-z]*$"), callback: StaticHelpers.getCommands()},
            {entryPoint: new RegExp("^(block)\\s[a-z]*$"), callback: StaticHelpers.getBlocks()},
            {entryPoint: new RegExp("^(monster)\\s[a-z]*$"), callback: StaticHelpers.getMonsters()},
            {entryPoint: new RegExp("^(remove)\\s[a-z]*$"), callback: StaticHelpers.getMonsters()},
            {entryPoint: new RegExp("^(new)\\s[a-z]*$"), callback: StaticHelpers.getBlocks()},
            {entryPoint: new RegExp("^(damage)\\s[a-z]*$"), callback: StaticHelpers.getMonsters()},
            {entryPoint: new RegExp("^(heal)\\s[a-z]*$"), callback: StaticHelpers.getMonsters()},
            // {entryPoint: new RegExp("^(electron)\\s[a-z]*$"), callback: StaticHelpers.getReleases()},
            {entryPoint: new RegExp("^[a-z]*$"), callback: StaticHelpers.getCommands()},
            {entryPoint: new RegExp("^(help)\\s[a-z]*$"), callback: StaticHelpers.getCommands()}
        ];
    }

}