import $ from 'jquery'
import Engine from "../initiative/Engine";

export default class GestionAPI {

    static async postCharacter(name, stats) {
        return await Engine.callServer("POST", "gestion", {"name": name, "stats": stats})
            .catch(error => {
                let statusCode = error.status;
                console.log(error);
                switch (statusCode) {
                    case 406 :
                        console.log("406");
                        return {
                            name: name,
                            stats: stats,
                            output: error.responseText
                        };

                    default:
                        return {
                            output: "Unexpected error " + statusCode,
                        };
                }
            })
    }

    static async getCharacters() {
        let charas =  await Engine.callServer("GET", "gestion", {});
        console.log(charas);
        return charas;
    }

}