import Vue from 'vue'
import App from './App.vue'
import Initiative from './components/initiative/Initiative'
import Index from './components/Index'
import VueRouter from "vue-router";
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import Engine from "./initiative/Engine"

useEslint: false;
Vue.config.productionTip = false;

Vue.use(VueRouter);
Vue.use(BootstrapVue);

const routes = [
    {path: '*', redirect: '/Gestion'},
    {path: '/Gestion', component: App},
    {path: '/Initiative', component: Initiative}
];

const router = new VueRouter({
    routes
});

window.app = new Vue({
    router,
    render: h => h(Index)
}).$mount('#app');

const evtSource = new EventSource(Engine.server + "stream");
evtSource.onmessage = function(e) {
    if (e.data !== "ServerSentEvent(,None,None,None)"){
        window.initiative.encounter = JSON.parse(e.data);
    }
};
